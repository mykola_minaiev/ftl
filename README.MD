# cypress-api

Cypress API tests

# Prerequisites

- npm

# Installation
```
npm intall
```
# Running cypress ui
You can specify environment before running tests using cypress ui(default is qa)

Default
```
npm start
```
Specifying environment
```
npm run-script start:qa
npm run-script start:prod
```
# Running cypress tests
You can specify environment before running tests using cypress ui(default is qa)

Default
```
npm test
```
Specifying environment
```
npm run-script test:qa
npm run-script test:prod
```

# backend

Spring boot app

# Prerequisites

- java 8+
- maven

# Running backend

mvn spring-boot:run
