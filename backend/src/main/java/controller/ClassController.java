package controller;

import entity.SchoolClass;
import entity.Student;
import entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import service.ClassService;
import service.StudentService;
import service.TeacherService;

import java.util.List;

@RestController
public class ClassController {

    @Autowired
    private ClassService classService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @PostMapping(value = "/class/create")
    public ResponseEntity<?> createClass(@RequestParam(value = "studentId") List<String> studentIds,
                                      @RequestParam(value = "teacherId") List<String> teacherIds,
                                      @RequestParam(value = "semester") String semester) {
        List<Student> students = studentService.findStudentsByIds(studentIds);
        List<Teacher> teachers = teacherService.findTeachersByIds(teacherIds);
        SchoolClass schoolClass = classService.createClass(students, teachers, semester);
        if(schoolClass != null) {
            return new ResponseEntity<>(schoolClass, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Class with such semester already exists", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
