package controller;

import entity.Question;
import entity.Quiz;
import entity.Student;
import entity.StudentQuizEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.QuizService;

import java.util.List;

@RestController
public class QuizController {

    @Autowired
    private QuizService quizService;

    @GetMapping(value = "/quizzes")
    public List<Quiz> getAllQuizzes() {
        return quizService.findAllQuizzes();
    }

    @PostMapping(value = "/quizzes/create")
    public ResponseEntity<?> createQuiz(@RequestParam(value = "name") String name,
                                        @RequestBody List<Question> questions) {
        Quiz quiz = quizService.createQuiz(questions, name);
        if (quiz != null) {
            return new ResponseEntity<>(quiz, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Quiz with such name already exists", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/quizzes/complete")
    public ResponseEntity<?> completeQuiz(@RequestParam(value = "quizId") String quizId,
                                          @RequestParam(value = "studentId") String studentId) {
        StudentQuizEntity sqe = quizService.completeQuiz(quizId, studentId);
        if (sqe != null) {
            return new ResponseEntity<>(sqe, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/quizzes/assign")
    public ResponseEntity<?> assignQuiz(@RequestParam(value = "quizId") String quizId,
                                        @RequestParam(value = "studentId") String studentId) {
        Student student = quizService.assignQuiz(quizId, studentId);
        if (student != null) {
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/quizzes/assign/multiple")
    public ResponseEntity<?> assignMultipleQuizzes(@RequestParam(value = "quizId") List<String> quizIds,
                                                   @RequestParam(value = "studentId") String studentId) {
        Student student = quizService.assignMultipleQuizzes(quizIds, studentId);
        if (student != null) {
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/quizzes/assign/multiple/students")
    public ResponseEntity<?> assignMultipleQuizzes(@RequestParam(value = "quizId") List<String> quizIds,
                                                   @RequestParam(value = "studentId") List<String> studentIds) {
        List<Student> students = quizService.assignMultipleQuizzesToMultipleStudents(quizIds, studentIds);
        if (students != null) {
            return new ResponseEntity<>(students, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/quizzes/grade")
    public ResponseEntity<?> gradeQuiz(@RequestParam(value = "quizId") String quizId,
                                       @RequestParam(value = "studentId") String studentId) {
        StudentQuizEntity sqe = quizService.gradeQuiz(quizId, studentId);
        if (sqe != null) {
            return new ResponseEntity<>(sqe, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
