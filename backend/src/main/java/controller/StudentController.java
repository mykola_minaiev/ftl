package controller;

import entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import service.StudentService;

import java.util.List;
import java.util.Map;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/students")
    public ResponseEntity<?> getAllStudents() {
        List<Student> students = studentService.findAllStudents();
        if (students != null) {
            return new ResponseEntity<>(students, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/students/create")
    public ResponseEntity<?> createStudent() {
        Student student = studentService.createStudent();
        if (student != null) {
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/students/grades")
    public ResponseEntity<?> getStudentsGradesOverSemester(@RequestParam(value = "semester", defaultValue = "1") String semester) {
        Map<Student, Integer> studentGradesOverSemester = studentService.calculateStudentsGradesOverSemester(semester);
        if (studentGradesOverSemester != null) {
            return new ResponseEntity<>(studentGradesOverSemester, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
