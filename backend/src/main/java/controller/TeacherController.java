package controller;

import entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import service.TeacherService;

@RestController
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @PostMapping(value = "/teachers/create")
    public ResponseEntity<?> createTeacher() {
        Teacher teacher = teacherService.createTeacher();
        if (teacher != null) {
            return new ResponseEntity<>(teacher, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
