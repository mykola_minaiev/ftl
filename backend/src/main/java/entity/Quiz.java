package entity;

import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.Objects;

public class Quiz {

    @Id
    private String id;
    private String name;
    private List<Question> questions;

    public Quiz(String name, List<Question> questions) {
        this.name = name;
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public String getId() {
        return id;
    }
}
