package entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;

public class SchoolClass {

    @Id
    private String id;
    @Indexed(unique = true)
    private String semester;
    private List<Student> students;
    private List<Teacher> teachers;

    public SchoolClass(List<Student> students, List<Teacher> teachers, String semester) {
        this.students = students;
        this.teachers = teachers;
        this.semester = semester;
    }

    public String getId() {
        return id;
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public String getSemester() {
        return semester;
    }
}
