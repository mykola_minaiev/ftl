package entity;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Student {

    @Id
    private String id;
    private List<Quiz> quizzes;

    public Student() {
        this.quizzes = new ArrayList<Quiz>();
    }

    public List<Quiz> getQuizzes() {
        return quizzes;
    }

    public void setQuizzes(List<Quiz> quizzes) {
        this.quizzes = quizzes;
    }

    public String getId() {
        return id;
    }
}
