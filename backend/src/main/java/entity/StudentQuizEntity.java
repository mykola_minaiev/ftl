package entity;

import org.springframework.data.annotation.Id;

public class StudentQuizEntity {

    @Id
    private String id;
    private int grade;
    private Student student;
    private Quiz quiz;

    public StudentQuizEntity(Student student, Quiz quiz) {
        this.student = student;
        this.quiz = quiz;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getId() {
        return id;
    }
}
