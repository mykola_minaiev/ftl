package entity;

import org.springframework.data.annotation.Id;

public class Teacher {

    @Id
    private String id;

    public String getId() {
        return id;
    }
}
