package repository;

import entity.SchoolClass;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepository extends MongoRepository<SchoolClass, String> {
    SchoolClass findBySemester(String semester);
}
