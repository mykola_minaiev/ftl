package repository;

import entity.Quiz;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends MongoRepository<Quiz, String> {
    @Query("{_id: { $in: ?0 } })")
    List<Quiz> findByIds(List<String> ids);

    Quiz findByName(String name);
}
