package repository;

import entity.Student;
import entity.StudentQuizEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentQuizRepository extends MongoRepository<StudentQuizEntity, String> {
    List<StudentQuizEntity> findByStudent(Student student);
}
