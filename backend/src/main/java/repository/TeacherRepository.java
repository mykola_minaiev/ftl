package repository;

import entity.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends MongoRepository<Teacher, String> {
    @Query("{_id: { $in: ?0 } })")
    List<Teacher> findByIds(List<String> ids);
}
