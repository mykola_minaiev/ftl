package service;

import entity.SchoolClass;
import entity.Student;
import entity.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClassService {

    SchoolClass createClass(List<Student> students, List<Teacher> teachers, String semester);
}
