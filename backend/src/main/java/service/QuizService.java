package service;

import entity.Question;
import entity.Quiz;
import entity.Student;
import entity.StudentQuizEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface QuizService {

    List<Quiz> findAllQuizzes();

    Quiz createQuiz(List<Question> questions, String name);

    StudentQuizEntity completeQuiz(String quizId, String studentId);

    Student assignQuiz(String quizId, String studentId);

    Student assignMultipleQuizzes(List<String> quizIds, String studentId);

    List<Student> assignMultipleQuizzesToMultipleStudents(List<String> quizIds, List<String> studentIds);

    StudentQuizEntity gradeQuiz(String quizId, String studentId);
}
