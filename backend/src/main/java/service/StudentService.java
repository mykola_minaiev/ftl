package service;

import entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface StudentService {

    List<Student> findAllStudents();

    List<Student> findStudentsByIds(List<String> studentIds);

    Student createStudent();

    Map<Student, Integer> calculateStudentsGradesOverSemester(String semester);
}
