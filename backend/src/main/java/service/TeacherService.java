package service;

import entity.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeacherService {

    Teacher createTeacher();

    List<Teacher> findTeachersByIds(List<String> teacherIds);
}
