package service.impl;

import entity.Quiz;
import entity.SchoolClass;
import entity.Student;
import entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.ClassRepository;
import service.ClassService;

import java.util.List;

@Service
public class ClassServiceImpl implements ClassService {

    @Autowired
    private ClassRepository classRepository;

    @Override
    public SchoolClass createClass(List<Student> students, List<Teacher> teachers, String semester) {
        SchoolClass originalSchoolClass = classRepository.findBySemester(semester);
        if (originalSchoolClass == null) {
            SchoolClass schoolClass = new SchoolClass(students, teachers, semester);
            classRepository.save(schoolClass);
            return classRepository.findBySemester(semester);
        }
        return null;
    }
}
