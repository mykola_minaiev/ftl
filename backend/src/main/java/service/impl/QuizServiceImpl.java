package service.impl;

import entity.Question;
import entity.Quiz;
import entity.Student;
import entity.StudentQuizEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.QuizRepository;
import repository.StudentQuizRepository;
import repository.StudentRepository;
import service.QuizService;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentQuizRepository studentQuizRepository;

    @Override
    public List<Quiz> findAllQuizzes() {
        return quizRepository.findAll();
    }

    public Quiz createQuiz(List<Question> questions, String name) {
        Quiz originalQuiz = quizRepository.findByName(name);
        if (originalQuiz == null) {
            Quiz quiz = new Quiz(name, questions);
            quizRepository.save(quiz);
            return quizRepository.findByName(name);
        }
        return null;
    }

    public StudentQuizEntity completeQuiz(String quizId, String studentId) {
        Quiz quiz = quizRepository.findById(quizId).orElse(null);
        Student student = studentRepository.findById(studentId).orElse(null);
        StudentQuizEntity studentQuizEntity = new StudentQuizEntity(student, quiz);
        studentQuizRepository.save(studentQuizEntity);
        List<StudentQuizEntity> studentQuizEntities = studentQuizRepository.findByStudent(student);
        Predicate<StudentQuizEntity> predicate = sqe -> sqe.getQuiz().getId().equals(quiz != null ? quiz.getId() : null);
        return studentQuizEntities.stream().filter(predicate).collect(Collectors.toList()).get(0);
    }

    public Student assignQuiz(String quizId, String studentId) {
        Quiz quiz = quizRepository.findById(quizId).orElse(null);
        Student student = studentRepository.findById(studentId).orElse(null);
        assert student != null;
        student.setQuizzes(Collections.singletonList(quiz));
        studentRepository.save(student);
        return studentRepository.findById(studentId).orElse(null);
    }

    public Student assignMultipleQuizzes(List<String> quizIds, String studentId) {
        List<Quiz> quizzes = quizRepository.findByIds(quizIds);
        Student student = studentRepository.findById(studentId).orElse(null);
        assert student != null;
        student.setQuizzes(quizzes);
        studentRepository.save(student);
        return studentRepository.findById(studentId).orElse(null);
    }

    public List<Student> assignMultipleQuizzesToMultipleStudents(List<String> quizIds, List<String> studentIds) {
        List<Quiz> quizzes = quizRepository.findByIds(quizIds);
        List<Student> students = studentRepository.findByIds(studentIds);
        for (Student student : students) {
            student.setQuizzes(quizzes);
            studentRepository.save(student);
        }
        return studentRepository.findByIds(studentIds);
    }

    public StudentQuizEntity gradeQuiz(String quizId, String studentId) {
        final Quiz quiz = quizRepository.findById(quizId).orElse(null);
        Student student = studentRepository.findById(studentId).orElse(null);
        List<StudentQuizEntity> studentQuizEntities = studentQuizRepository.findByStudent(student);
        Predicate<StudentQuizEntity> predicate = studentQuizEntity -> studentQuizEntity.getQuiz().getId().equals(quiz != null ? quiz.getId() : null);
        StudentQuizEntity studentQuizEntity = studentQuizEntities.stream().filter(predicate).collect(Collectors.toList()).get(0);
        if (studentQuizEntity.getGrade() > 0) {
            throw new RuntimeException("Quiz is already graded");
        }
        studentQuizEntity.setGrade(new Random().nextInt(10) + 1);
        studentQuizRepository.save(studentQuizEntity);
        List<StudentQuizEntity> sqes = studentQuizRepository.findByStudent(student);
        return sqes.stream().filter(predicate).collect(Collectors.toList()).get(0);
    }
}
