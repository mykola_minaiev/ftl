package service.impl;

import entity.SchoolClass;
import entity.Student;
import entity.StudentQuizEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.ClassRepository;
import repository.QuizRepository;
import repository.StudentQuizRepository;
import repository.StudentRepository;
import service.StudentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private StudentQuizRepository studentQuizRepository;

    @Override
    public List<Student> findAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public List<Student> findStudentsByIds(List<String> studentIds) {
        return studentRepository.findByIds(studentIds);
    }

    @Override
    public Student createStudent() {
        Student student = new Student();
        student = studentRepository.save(student);
        return student;
    }

    public Map<Student, Integer> calculateStudentsGradesOverSemester(String semester) {
        Map<Student, Integer> studentGradesMap = new HashMap<Student, Integer>();
        SchoolClass schoolClass = classRepository.findBySemester(semester);
        for (Student student : schoolClass.getStudents()) {
            List<StudentQuizEntity> studentQuizEntities = studentQuizRepository.findByStudent(student);
            int summaryGrade = 0;
            for (StudentQuizEntity sqe : studentQuizEntities) {
                summaryGrade += sqe.getGrade();
            }
            studentGradesMap.put(student, summaryGrade);
        }
        return studentGradesMap;
    }
}
