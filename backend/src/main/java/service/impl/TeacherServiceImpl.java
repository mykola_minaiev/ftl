package service.impl;

import entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.TeacherRepository;
import service.TeacherService;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public Teacher createTeacher() {
        Teacher teacher = new Teacher();
        teacher = teacherRepository.save(teacher);
        return teacher;
    }

    @Override
    public List<Teacher> findTeachersByIds(List<String> teacherIds) {
        return teacherRepository.findByIds(teacherIds);
    }
}
