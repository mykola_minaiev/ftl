describe("Seed data", function () {
    it('Create student', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/students/create`
        }).then(response => {
            console.log(response.body.id);
            Cypress.env("firstStudentId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create student', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/students/create`
        }).then(response => {
            Cypress.env("secondStudentId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create student', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/students/create`
        }).then(response => {
            Cypress.env("thirdStudentId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create student', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/students/create`
        }).then(response => {
            Cypress.env("fourthStudentId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create teacher', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/teachers/create`
        }).then(response => {
            Cypress.env("teacherId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/create`,
            qs: {
                name: "initQuiz"
            },
            body: [
                {
                    question: "What is your name?"
                },
                {
                    question: "How old are you?"
                }
            ]
        }).then(response => {
            Cypress.env("firstQuizId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/create`,
            qs: {
                name: "initQuiz2"
            },
            body: [
                {
                    question: "What is your name?2"
                },
                {
                    question: "How old are you?2"
                }
            ]
        }).then(response => {
            Cypress.env("secondQuizId", response.body.id);
            expect(response.status).to.equal(200);
        });
    });

    it('Create class', function () {
        console.log(Cypress.env("firstStudentId"));
        console.log(Cypress.env("secondStudentId"));
        console.log(Cypress.env("teacherId"));
        console.log(Cypress.env("firstQuizId"));
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/class/create`,
            qs: {
                studentId: Cypress.env("firstStudentId"),
                studentId: Cypress.env("secondStudentId"),
                teacherId: Cypress.env("teacherId"),
                semester: Cypress.env("semester")
            }
        }).then(response => {
            expect(response.status).to.equal(200);
        });
    });
});
describe("FineTune positive cases", function () {
    it('Assign quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/assign`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                studentId: Cypress.env("firstStudentId")
            }
        }).then(response => {
            expect(response.status).to.equal(200);
        });
    });

    it('Assign multiple quizzes', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/assign/multiple`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                quizId: Cypress.env("secondQuizId"),
                studentId: Cypress.env("secondStudentId")
            }
        }).then(response => {
            expect(response.status).to.equal(200);
        });
    });

    it('Assign multiple quizzes to multiple students', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/assign/multiple`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                quizId: Cypress.env("secondQuizId"),
                studentId: Cypress.env("thirdStudentId"),
                studentId: Cypress.env("fourthStudentId")
            }
        }).then(response => {
            expect(response.status).to.equal(200);
        });
    });

    it('Complete quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/complete`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                studentId: Cypress.env("firstStudentId")
            }
        }).then(response => {
            expect(response.body.grade).to.equal(0);
            expect(response.status).to.equal(200);
        });
    });

    it('Grade quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/grade`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                studentId: Cypress.env("firstStudentId")
            }
        }).then(response => {
            expect(response.status).to.equal(200);
        });
    });

    it('Get students grades for semester', function () {
        cy.request({
            method: 'GET',
            url: `${Cypress.env('baseUrl')}/students/grades`,
            qs: {
                semester: Cypress.env("semester")
            }
        }).then(response => {
            expect(response.body.grade).to.not.equal(0);
            expect(response.status).to.equal(200);
        });
    });
});

describe("FineTune negative cases", function () {
    it('Assign quiz to non-existing student', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/assign`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                studentId: "non-existingId"
            },
            failOnStatusCode: false
        }).then(response => {
            expect(response.status).to.equal(500);
        });
    });

    it('Assign multiple quizzes to non-existing student', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/assign/multiple`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                quizId: Cypress.env("secondQuizId"),
                studentId: "non-existingId"
            },
            failOnStatusCode: false
        }).then(response => {
            expect(response.status).to.equal(500);
        });
    });

    it('Assign multiple quizzes to multiple students one of them is non-existing', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/assign/multiple`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                quizId: Cypress.env("secondQuizId"),
                studentId: Cypress.env("thirdStudentId"),
                studentId: "non-existingId"
            },
            failOnStatusCode: false
        }).then(response => {
            expect(response.status).to.equal(500);
        });
    });

    it('Complete already completed quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/complete`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                studentId: Cypress.env("firstStudentId")
            }
        }).then(response => {
            expect(response.body.grade).to.not.equal(0);
            expect(response.status).to.equal(200);
        });
    });

    it('Grade quiz', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/grade`,
            qs: {
                quizId: Cypress.env("firstQuizId"),
                studentId: Cypress.env("firstStudentId")
            },
            failOnStatusCode: false
        }).then(response => {
            expect(response.body.message).to.equal("Quiz is already graded");
            expect(response.status).to.equal(500);
        });
    });

    it('Get students grades for non-existing semester', function () {
        cy.request({
            method: 'GET',
            url: `${Cypress.env('baseUrl')}/students/grades`,
            qs: {
                semester: "non-existing semester"
            },
            failOnStatusCode: false
        }).then(response => {
            expect(response.status).to.equal(500);
        });
    });

    it('Create quiz with duplicated name', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/quizzes/create`,
            qs: {
                name: "initQuiz"
            },
            body: [
                {
                    question: "What is your name?"
                },
                {
                    question: "How old are you?"
                }
            ],
            failOnStatusCode: false
        }).then(response => {
            expect(response.body).to.equal("Quiz with such name already exists");
            expect(response.status).to.equal(404);
        });
    });

    it('Create class with duplicated semester', function () {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('baseUrl')}/class/create`,
            qs: {
                studentId: Cypress.env("firstStudentId"),
                studentId: Cypress.env("secondStudentId"),
                teacherId: Cypress.env("teacherId"),
                semester: Cypress.env("semester")
            },
            failOnStatusCode: false
        }).then(response => {
            expect(response.status).to.equal(500);
        });
    });
});